Source: golang-github-foxboron-go-uefi
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders:
 Simon Josefsson <simon@josefsson.org>,
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-golang,
 golang-any,
 golang-github-fullsailor-pkcs7-dev,
 golang-github-netflix-go-expect-dev,
 golang-github-pkg-errors-dev,
 golang-github-spf13-afero-dev,
 golang-golang-x-crypto-dev,
 golang-golang-x-sys-dev,
 golang-golang-x-text-dev,
 openssl,
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-foxboron-go-uefi
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-foxboron-go-uefi.git
Homepage: https://github.com/foxboron/go-uefi
XS-Go-Import-Path: github.com/foxboron/go-uefi

Package: golang-github-foxboron-go-uefi-dev
Architecture: all
Multi-Arch: foreign
Depends:
 golang-github-pkg-errors-dev,
 golang-github-spf13-afero-dev,
 golang-golang-x-crypto-dev,
 golang-golang-x-sys-dev,
 golang-golang-x-text-dev,
 ${misc:Depends},
Description: Linux UEFI library written in pure Go (library)
 A UEFI library written to interact with Linux efivars. The goal is to
 provide a Go library to enable application authors to better utilize
 secure boot and UEFI. This also includes unit-testing to ensure the
 library is compatible with existing tools, and integration tests to
 ensure the library is able of deal with future UEFI revisions.
 .
 Features:
  * Implements most Secure Boot relevant structs as defined in UEFI Spec
    Version 2.8 Errata A (February 14th 2020).
  * PE/COFF Checksumming.
  * Microsoft Authenticode signing.
  * Working with EFI_SIGNATURE_LIST and EFI_SIGNATURE_DATABASE.
  * Integration tests utilizing vmtest (https://github.com/anatol/vmtest)
    and tianocore.
  * Virtual filesystem support for easier testing.
